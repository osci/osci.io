---
title: Network
---

|       VLANs       | ID  |    IP Ranges     |             Gateway             | Purpose                                                |
| :---------------: | :-: | :--------------: | :-----------------------------: | :----------------------------------------------------- |
|    OSCI-Public    | 190 | 8.43.85.192-239  |           8.43.85.254           | direct Internet access                                 |
|        ^^         |     | 2620:52:3:1::/64 | 2620:52:3:1:ffff:ffff:ffff:fffe |                                                        |
| OSCI-Provisioning | 430 | 172.24.30.1-249  |          172.24.30.254          | provisioning of new hosts                              |
|  OSCI-Management  | 431 | 172.24.31.1-249  |          172.24.31.254          | equipments administration                              |
|   OSCI-Internal   | 432 | 172.24.32.1-249  |          172.24.32.254          | machines behind a reverse proxy (security, saves IPs…) |
| OSCI-Tenants-Mgmt | 443 | 172.24.43.1-249  |          172.24.43.254          | Shared Management VLAN for small/medium tenant         |
|   (unassigned)    | 444 | 172.24.44.1-249  |          172.24.44.254          | Management VLAN for a future tenant                    |
