---
title: Tenants
---

## Embedded Communities

Embedded communities are tenants with whom we work closely on their infrastructure.

|           Tenant            |                            Involvement                            |
| :-------------------------: | :---------------------------------------------------------------: |
|           Beaker            |                 member of the infrastructure team                 |
|           Gluster           |                  lead of the infrastructure team                  |
|             RDO             |                 member of the infrastructure team                 |
|            oVirt            |                 member of the infrastructure team                 |
| Project Atomic / Silverblue | lead of the infrastructure team<br/>working with Fedora sysadmins |
|            Pulp             |                 member of the infrastructure team                 |
|    Theopensourceway.org     |                        infrastructure team                        |

## Tenants of the Community Cage

The following tenants are hosted in the Community Cage (co-location services for various community projects):

- CentOS
- Ceph
- Fedora
- Gluster
- GNOME
- Sourceware
- Trystack
- Wildfly

## Hosted Tenants

We provide VMs, containers, and services to Hosted Tenants using the OSCI infrastructure.

|         Tenant         |          Service           |                      Location                      |                            Description                            | Level of<br/>Involvement | Status |
| :--------------------: | :------------------------: | :------------------------------------------------: | :---------------------------------------------------------------: | :----------------------: | :----: |
|         Beaker         |      Website / Mails       | www.beaker-project.org<br/>mail.beaker-project.org |  Webserver with rsync uploads for website and RPMs, mail aliases  |      collaboration       |   OK   |
|       BugHunting       |          Website           |                 www.bughunting.cz                  |                                                                   |       hosting-only       |   OK   |
|          Ceph          |         File zone          |               nextcloud-ceph.osci.io               |                             Nextcloud                             |           full           |   OK   |
|     Crash Utility      |            MLs             |            lists.crash-utility.osci.io             |                             Mailman 3                             |           full           |   OK   |
|        Devconf         |           Matrix           |                 matrix.devconf.cz                  |                         Synapse and bots                          |           full           |   OK   |
|       DogTag PKI       |            MLs             |                lists.dogtagpki.org                 |                             Mailman 3                             |           full           |   OK   |
|     Fedora Atomic      |         Test nodes         |            fedora-atomic-[1-6].osci.io             |                    dedicated blades for tests                     |      collaboration       |   OK   |
|          GDB           |           Gerrit           |            gnutoolchain-gerrit.osci.io             |                                                                   |       hosting-only       |   OK   |
|           ^^           |       Build machine        |                gdb-buildbot.osci.io                |                             Buildbot                              |       hosting-only       |   OK   |
|        Gnocchi         |          Website           |                  gnocchi.osci.io                   | Gnocchi & client documentation builder and web server, web server |           full           |   OK   |
|        Heptapod        |             CI             |                  heptapod.osci.io                  |                    dedicated blade for CI node                    |       hosting-only       |   OK   |
|         JBOSS          |            MLs             |                  lists.jboss.org                   |                             Mailman 3                             |           full           |   OK   |
|          KDLP          |             CI             |      {prod,dev}-01.kdlp.underground.software       |                                                                   |       hosting-only       |   OK   |
|         Kiali          | OpenShift Service Mesh bot |                 kiali-bot.osci.io                  |                                                                   |       hosting-only       |   OK   |
|          Koji          |      Website Builder       |            koji-web-builder.int.osci.io            |                 Nikola builder for www.koji.build                 |           full           |   OK   |
|           ^^           |          Website           |                   www.koji.build                   |                             Webserver                             |           full           |   OK   |
|          KVM           |          Website           |                   linux-kvm.org                    |                                                                   |       hosting-only       |   OK   |
|       Libguestfs       |            MLs             |                lists.libguestfs.org                |                             Mailman 3                             |           full           |   OK   |
|        Libvirt         |            MLs             |                 lists.libvirt.org                  |                             Mailman 3                             |           full           |   OK   |
|      Linux Audit       |            MLs             |             lists.linux-audit.osci.io              |                             Mailman 3                             |           full           |   OK   |
|       Minishift        |            MLs             |                 lists.minishift.io                 |                             Mailman 3                             |           full           |   OK   |
| Enterprise Neurosystem |            MLs             |           www.enterpriseneurosystem.org            |                             Webserver                             |           full           |  WIP   |
|           ^^           |            MLs             |   enterpriseneurosystem-web-builder.int.osci.io    |         Jekyll builder for www.enterpriseneurosystem.org          |           full           |  WIP   |
|           ^^           |            MLs             |          lists.enterpriseneurosystem.org           |                             Mailman 3                             |           full           |  WIP   |
|      NFS Ganesha       |            MLs             |               lists.nfs-ganesha.org                |                             Mailman 3                             |           full           |   OK   |
|           ^^           |      Web redirection       |                www.nfs-ganesha.org                 |                    redirection to GitHub wiki                     |           full           |   OK   |
|           ^^           |         File zone          |              download.nfs-ganesha.org              |                    Webserver with SFTP uploads                    |           full           |   OK   |
|     Open Data Hub      |            MLs             |                lists.opendatahub.io                |                             Mailman 3                             |           full           |   OK   |
|   Open Source Infra    |            MLs             |             lists.opensourceinfra.org              |                             Mailman 3                             |           full           |   OK   |
|       OSBusiness       |            MLs             |                lists.osbusiness.org                |                             Mailman 3                             |           full           |   OK   |
|        OpenJDK         |          Sources           |              openjdk-sources.osci.io               |                    Webserver and SFTP accounts                    |           full           |   OK   |
|     Operate First      |            MLs             |             lists.operate-first.cloud              |                             Mailman 3                             |           full           |   OK   |
|       OSPO/OSCI        |         Analytics          |                  cauldron.osci.io                  |                         Cauldron instance                         |       hosting-only       |   OK   |
|           ^^           |     Jupyter Notebooks      |                  jupyter.osci.io                   |                   Data science and AI research                    |           full           |   OK   |
|         oVirt          |            MLs             |                   mail.ovirt.org                   |               Mailman 3, web UI on lists.ovirt.org                |      collaboration       |   OK   |
|           ^^           |      Website Builder       |           ovirt-web-builder.int.osci.io            |                Middleman builder for www.ovirt.org                |      collaboration       |   OK   |
|           ^^           |          Website           |                   www.ovirt.org                    |                             Webserver                             |      collaboration       |   OK   |
|           ^^           |         Monitoring         |                monitoring.ovirt.org                |                              Icinga                               |      collaboration       |   OK   |
|           ^^           |           Glance           |                  glance.ovirt.org                  |                          Glance instance                          |      collaboration       |   OK   |
|       Patternfly       |           Forum            |              patternfly-forum.osci.io              |                             Discourse                             |      collaboration       |   OK   |
|          PCP           |           Tools            |                                                    |                    Taskboard and various tools                    |       hosting-only       |   OK   |
|          PO4A          |          Website           |                    www.po4a.org                    |           Jekyll or ASCII Binder builder and webserver            |           full           |   OK   |
|           ^^           |            MLs             |                   lists.po4a.org                   |                             Mailman 3                             |           full           |   OK   |
|         Podman         |            MLs             |        lists.podman.io<br/>lists.buildah.io        |            Mailman 3, shared with the Buildah project             |           full           |   OK   |
|          Pulp          |      Website Builder       |            pulp-web-builder.int.osci.io            |                Jekyll builder for pulpproject.org                 |      collaboration       |   OK   |
|           ^^           |          Websites          |      pulpproject.org<br/>docs.pulpproject.org      |           Webserver for project pages and documentation           |      collaboration       |   OK   |
|         Python         |          Builder           |           python-builder-rawhide.osci.io           |                            CI Builder                             |       hosting-only       |   OK   |
|           ^^           |          Builder           |          python-builder2-rawhide.osci.io           |                            CI Builder                             |       hosting-only       |   OK   |
|           ^^           |          Builder           |            python-builder-rhel7.osci.io            |                            CI Builder                             |       hosting-only       |   OK   |
|           ^^           |          Builder           |            python-builder-rhel8.osci.io            |                            CI Builder                             |       hosting-only       |   OK   |
|           ^^           |          Builder           |         python-builder-rhel8-fips.osci.io          |                            CI Builder                             |       hosting-only       |   OK   |
|          RDO           |      Website Builder       |            rdo-web-builder.int.osci.io             |             Middleman builder for www.rdoproject.org              |      collaboration       |   OK   |
|           ^^           |            MLs             |                lists.rdoproject.org                |                             Mailman 3                             |      collaboration       |  WIP   |
|       Shipwright       |            MLs             |                lists.shipwright.io                 |                             Mailman 3                             |           full           |   OK   |
|   Software Heritage    |        Computation         |              Indexing/Processing node              |                          dedicated blade                          |       hosting-only       |  PLAN  |
|         Spice          |          Website           |                www.spice-space.org                 |                             Webserver                             |           full           |   OK   |
|   The ChRIS Project    |            MLs             |               lists.chrisproject.org               |                             Mailman 3                             |           full           |   OK   |
|  The Open Source Way   |          Website           |                theopensourceway.org                |                             Mediawiki                             |           full           |   OK   |
|         Zanata         |      Translation Tool      |                translate.zanata.org                |                          Zanata instance                          |           full           |   OK   |

## Externaly Hosted Tenants

We also provide services using third-party plateforms to complement our own infrastructure.

|           Tenant           |      Service       |            Location             |     Third-Party     | Status |
| :------------------------: | :----------------: | :-----------------------------: | :-----------------: | :----: |
|          Ansible           |      Website       |         www.ansible.com         | OpenShift Dedicated |   OK   |
|             ^^             |     patchback      |                                 | OpenShift Dedicated |   OK   |
|          Apicurio          |                    |                                 | OpenShift Dedicated |   OK   |
| Conscious Language Checker |      CLC Demo      |           clc.osci.io           | OpenShift Dedicated |   OK   |
|  Creative Freedom Summit   |      Website       |    creativefreedomsummit.com    |      WPEngine       |   OK   |
|        Carbon Ninja        |                    |                                 | OpenShift Dedicated |   OK   |
|         Devconf.cz         |                    |                                 | OpenShift Dedicated |   OK   |
|          DuckCorp          |  LDAPWalker Demo   |  ldapwalker-demo.duckcorp.org   | OpenShift Dedicated |   OK   |
|         Eightknot          |                    |                                 | OpenShift Dedicated |   OK   |
|   Enterprise Neurosystem   |      Website       |  www.enterpriseneurosystem.org  |      WPEngine       |   OK   |
|          Fabric8           |         CI         |                                 | OpenShift Dedicated |   OK   |
|           Fedora           |        Blog        | communityblog.fedoraproject.org |      WPEngine       |   OK   |
|             ^^             |        Blog        |       fedoramagazine.org        |      WPEngine       |   OK   |
|             ^^             |      Podcasts      |    podcast.fedoraproject.org    |      WPEngine       |   OK   |
|          Foreman           |      Redmine       |     projects.theforeman.org     | OpenShift Dedicated |   OK   |
|             ^^             |     GitHub Bot     |   prprocessor.theforeman.org    | OpenShift Dedicated |   OK   |
|          Gluster           |   Website + Blog   |         www.gluster.org         |      WPEngine       |   OK   |
|             ^^             | softserve PY3 port |                                 | OpenShift Dedicated |   OK   |
|           Istio            |       Electo       |       elections.istio.io        | OpenShift Dedicated |   OK   |
|           JBOSS            |      Website       |         forge.jboss.org         | OpenShift Dedicated |   OK   |
|           JKUBE            |         CI         |                                 | OpenShift Dedicated |   OK   |
|           JOSDK            |                    |                                 | OpenShift Dedicated |   OK   |
|          KeyCloak          |     GitHub Bot     |                                 | OpenShift Dedicated |   OK   |
|          Knative           |       Electo       |      elections.knative.dev      | OpenShift Dedicated |   OK   |
|          Open Org          |      HedgeDoc      |  notes.theopenorganization.org  | OpenShift Dedicated |   OK   |
|           oVirt            |        Blog        |         blogs.ovirt.org         |      WPEngine       |   OK   |
|            OSPO            |      Website       |         next.redhat.com         |      WPEngine       |   OK   |
|           Podman           |      Website       |         blog.podman.io          |      WPEngine       |   OK   |
|       Project Atomic       |      Website       |      www.projectatomic.io       | OpenShift Dedicated |   OK   |
|            Pulp            |     Artifacts      |    fixtures.pulpproject.org     | OpenShift Dedicated |   OK   |
|             ^^             |     Discourse      |    discourse.pulpproject.org    | OpenShift Dedicated |   OK   |
|             ^^             |     Analytics      |                                 | OpenShift Dedicated |   OK   |
|          Quarkus           | Container Registry |       registry.quarkus.io       | OpenShift Dedicated |   OK   |
|             ^^             |      Website       |        status.quarkus.io        | OpenShift Dedicated |   OK   |
|             ^^             |      Lottery       |                                 | OpenShift Dedicated |   OK   |
|             ^^             |        Game        |                                 | OpenShift Dedicated |   OK   |
|            RDO             |        Blog        |      blogs.rdoproject.org       |      WPEngine       |   OK   |
|          Red Hat           |      Website       |        arcade.redhat.com        | OpenShift Dedicated |   OK   |
|             ^^             |   Website + Blog   |         next.redhat.com         |      WPEngine       |   OK   |
|             ^^             |      Website       |       research.redhat.com       |      WPEngine       |   OK   |
|          Wildfly           |     GitHub Bot     |                                 | OpenShift Dedicated |   OK   |
