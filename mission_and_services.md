---
title: OSPO CommInfra Services
---

## Our Mission

The OSPO Community Infrastructure (CommInfra) team seeks to provide infrastructure support to various [opensource communities fostered by Red Hat](/tenants/).

To achieve this we manage several projects and various resources described below.

### Community Cage

The _Community Cage_ project provides externally-facing co-location services to various community projects (CentOS, Fedora, Gluster, Ceph…). In the RDU2 datacenter in Raleigh we host projects owning their own hardware and provide space and connectivity. This is a joint effort between OSPO CommInfra, Red Hat IT, and PnT DevOps. The Cage is IPv6 enabled.

### OSCI Services

To provide infrastructure for more granular needs, or for smaller communities without their own hardware infrastructure, OSPO created the OSCI project. OSCI is a tenant of the Community cage and provide:

- shared services, used by multiples communities
- hosted services for small tenants or tenants in need of complementary services

All services are made using Free software.

### Externalized Services

Because some services are best maintained by external providers or because our small team lacks the resources to do it properly, we sometimes rely on external providers; this includes:

- VMs
- Openshift containers
- Wordpress instances

## Services Offering

The list of provided services is flexible and expanding with time and new projects.

Here is a non-exhaustive list of services we are used to provide and can deploy in a reasonable time:

|  Category  | Service Description                                                                               |
| :--------: | :------------------------------------------------------------------------------------------------ |
| Containers | OpenShift-hosted projects/containers                                                              |
|   Domain   | \* primary DNS: [Delegated DNS](/offers/dns4tenants/) or dynamic zones                            |
|     ^^     | \* secondary DNS servers (transfers secured using TSIG)                                           |
|    Mail    | \* mail server (redirections, mailboxes with IMAPS/POP3S access)                                  |
|     ^^     | \* [mailing-lists (using Mailman 3, migration from Mailman 2 possible)](/offers/mailing-lists/)   |
|     ^^     | \* anti-virus and anti-SPAM                                                                       |
|     ^^     | \* secondary MX servers                                                                           |
|    Time    | time server (NTP) accessible inside the Community Cage                                            |
|     VM     | Raw VM with root access                                                                           |
|    Web     | \* web sites hosting (static preferred, Ruby/Python/PHP/NodeJS possible)                          |
|     ^^     | \* HTTPS (using Let's Encrypt or dedicated certificates) and security settings (headers, CSP…)    |
|     ^^     | \* builders for various static sites generators (Ascii Binder, Jekyll, Middleman, Nikola, Planet) |
|     ^^     | \* Discourse (WIP)                                                                                |
|     ^^     | \* Nextcloud (WIP)                                                                                |
|     ^^     | \* Wordpress instances                                                                            |
|  Jumphost  | Access to tenants' machines in the Community Cage or VMs in our internal VLAN                     |

More generally we help tenants build their infra and customize it using various resources (ours, project owned, external). We can then fully manage it, collaboratively work together, or pass the knowledge over; it is flexible.

## Services Management

Shared services are fully managed by the OSPO Community Infrastructure (CommInfra) team, whereas hosted projects are handled according to project members' wishes, from raw VM with root access to full management by the OSPO team and anything in the middle.

The [infrastructure "code"](/infra_admin/) is public and we much welcome contributions.
