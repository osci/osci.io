---
title: Underlying Technical Services
---

|        Service        |                       Location                       |                                     Description                                     | Status |
| :-------------------: | :--------------------------------------------------: | :---------------------------------------------------------------------------------: | :----: |
|      CI Runners       |           gl-runner-podman-0{1-2}.osci.io            |                                  GitLab CI Runner                                   |   OK   |
|          MX1          |                    polly.osci.io                     |                         Mail redirections, RT mail accounts                         |   OK   |
|          MX2          | polly.osci.io<br/>francine.osci.io<br/>carla.osci.io |                     backup mail server for OSCI and communities                     |   OK   |
|          NS1          |                    polly.osci.io                     |                                 DNS primary server                                  |   OK   |
|          NS2          | polly.osci.io<br/>francine.osci.io<br/>carla.osci.io |                   DNS secondary servers for OSCI and communities                    |   OK   |
|          NTP          |            ntp1.osci.io<br/>ntp2.osci.io             |                      NTP stratum 2 server using a CDMA device                       |   OK   |
|          VMs          |          spritz.osci.io<br/>badcat.osci.io           |                                     hypervisor                                      |   OK   |
|        Storage        |                 lucille.srv.osci.io                  |             NFS server providing extra remote storage for VMs or blades             |   OK   |
|   Supervision / PXE   |                    catton.osci.io                    | Monitoring, DHCP/TFTP, netboot menu for various distros, manual/unattended installs |   OK   |
| Web Redirection / PXE |                     www.osci.io                      |                  APEX or other web redirections, PXE netboot/DVDs                   |   OK   |
