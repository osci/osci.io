---
title: Token creation for Mailing-lists authentication
---

## GitLab

On Gitlab, once logged, click on your profile account and select
_Preferences_. In the left menu got to _Applications_ and click on
_Add new application_.

You then need to fill-in a small form:

- Name: `Projectname` Mailing-Lists
- Redirect URI: https://`lists.example.org`/accounts/gitlab/login/callback/
- Confidential: checked
- Scopes: read_user

Get the application ID and secret and send them to us securely.

## GitHub

On GitHub, once logged, click on your profile account (top right) and
select _settings_. Go to _Developer settings_ / _OAuth applications_, then
click _Register Application_.

You then need to fill-in a small form:

- Application name: `Projectname` Mailing-Lists
- Homepage URL: https://`lists.example.org`/
- Application description: (you may leave empty)
- Authorization callback URL: https://`lists.example.org`/accounts/github/login/callback/

Get the client ID and client secret and send them to us securely.

## Google

On Google, once logged, go to this URL:
https://console.developers.google.com/apis/credentials

Create a new project _Mailing Lists_. Then I'm not sure of the order but
the settings follows.

In _OAuth consent screen_ fill-in the form:

- Email address: (contact email of your choice)
- Product name shown to users: `Projectname` Mailing-Lists
- Homepage URL: https://`lists.example.org`/

(you may leave the rest empty)

In _Credentials_ click on _Create credentials_, _OAuthclient ID_ and
_Web application_:

- Name: Mailman
- Authorized redirect URIs: https://`lists.example.org`/accounts/google/login/callback/

(you may leave the rest empty)

Get the client ID and client secret and send them to us securely.
