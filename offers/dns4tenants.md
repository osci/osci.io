---
title: Delegated DNS Service (DNS4Tenants)
---

* TOC
{:toc}

## Goal

Red Hat can host DNS for supported projects but what Red Hat IT propose is not always adequate for community use or sufficient, so we designed our own system. We wanted communities to be able to manage zone changes as they see fit and allow non Red Hatters can contribute to system administration tasks.

## Service Description

### Zone Update Mechanism and CI Integration

[DNS4Tenants](https://gitlab.com/osci/ansible-role-dns4tenants) is a simple simple system that runs on our primary DNS server and publishes your zones. You can host them in any git repository, public or private (in this case you only need to give read-only access to the service SSH key). This way a community can manage their workflow and give access to infra team members without having to wait for any kind of moderation.

The system will check for changes regularly and validate the zone before publishing. If you host your zones on Gitlab or Github, we provide a [CI configuration](https://gitlab.com/osci/dns4tenants-validation-container) to easily check your zones and PRs.

### Zone Location and Format

Zone files are named after the domain and can be located in a subdirectory of the git repository if you want to intergrate then with other infrastructure content.

The zone must be in Bind9 format.

### Secondary DNS Servers

We can provide secondary servers using our two other DNS servers, with one being in a different hosting location, or we can configure synchronization with any provider of your liking. Zone transfers must be protected with TSIG, that's the only constraint.

## Integration with Red Hat IT DNS

It is possible to take advantage of the DNS system that Red Hat IT provides and integrate it with DNS4Tenants. We discuss below how this is possible and what led to these architecture choices.

Red Hat IT replaced the old dnsmaps system and is now leveraging NS1.com services. Zone changes can now be done in multiple ways:

- use their repository to prepare a change to their Ansible rules and ask for moderation
- use NS1.com API directly
- use NS1.com ability to synchronize from a primary server

We did not want to have to wait anymore for changes to be moderated, knowing we do not work in the same timezone. Moreover IT repository access is limited to Red Hatters.

We considered using the API directly and using Ansible is a pretty neat choice but not everybody is used to write Ansible rules and you have to be careful that to remove records you need to actively add a rule for it and not simply delete the obsolete rule. The format is also extremely verbose and you may not be able to use all possible record types.

Using an API that is not a standard also ties you to a specific provider and if IT decides to change for any reason we would have to make all the changes for all communities by ourselves, but we are a very small team. It is also more community friendly to stay on a standard format and, if Red Hat was to hand over the DNS management, would make the transition much easier.

We decided to keep the usual Bind9 format that most people are used too and opt for the synchronize method. Moreover the zone file can also be validated globally and checked in a CI, which is not something possible using the API.

Coupling DNS4Tenants delegation mechanism, a well know format, validation, and the NS1.com synchronize method to take advantage of their geographic and redundancy coverage we believe this makes it a nice combination.
