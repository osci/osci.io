---
title: "Creating and Packaging a Jekyll Web Template for Open Source Communities"
date: 2022-10-07
author: Duck
---

[My team](https://osci.io), as sysadmins, builds solutions for our open source
communities. This includes various kinds of tasks but regularly we are asked to
help do more "simple" things like website deployment.

In fact the web can be deceitfully complex and as we're not web designers we try
to find ways to simplify our work. Many of the projects we work with use Jekyll 
for their web sites, so we needed something that could be set up fast without deep 
Jekyll or Ruby knowledge.

We'll be exploring the latest work we did on this topic.

READMORE

## Intern to the rescue

To help us we had an intern, Dinesh Kumar Ravikumar, give us a hand and write
[an easy to use template for Jekyll](https://github.com/DineshkumarRavikumar/animated-dollop).

It has all the basic setup and layout but allows for customization. You get
blog and calendar with feeds generation and more. And of course documentation.

## Going towards production

A few things needed fixing and improving to suit our needs and once done we
wondered how to use it in practice. If we are to share this template across
multiple websites then we need a way to apply changes without breaking the
world.

When you wish to share code among various programs the usual way is to build a
library, an API, and adapt all programs to use it. A website is a bit different
but with Jekyll it is possible to use gems to provide a theme. you cannot
provide web pages but layouts, templates, various assets and even the default
Jekyll configuration can be shared using a gem.

The documentation on how to achieve this was lacking at best but looking at
other gems we were able to figure it out. We [published the result](https://gitlab.com/osci/website-jekyll-base-gem)
using the Expat/MIT license. The source includes a test website used by the CI
which also acts as an example on how to effectively use this gem. Enjoy!
