---
title: Host Network Setup
---

## Introduction

After an installation, an automatic one especially, a host needs proper network settings before being provisioned like any other hosts via Ansible common rule.

The goal is to ensure:

- defining the host's hostname (most needed after an automatic installation using a generic name)
- setup of the network, taking advantage of the switches redundancy if possible (bonding using active-backup ; balance-alb was tested but did not work), with VLAN and bridging support

This method is able to configure Red hat and Debian systems. EL < 7 are configured using the ifcfg method while other systems are handled with NetworkManager keyfiles. This allows for a single method to configure all hosts (once older systems are upgraded). For aggregated interfaces older systems use bonding with active/backup method and newer systems use teaming with round robin method.

This method supports all interface naming schemes.

IPv6 is supported when using the NetworkManager method.

## Generated Network Setup

The playbook will first gather all known ways to contact the machine (inventory hostname, inventory IP…). it will then try them one by one to be able to connect via SSH using a temporary Ansible host.

In the case of blades, it will also fetch hardware information on the chasis to get the MAC addresses associated with this specific blade. Then it will search them in the DHCP server's ARP table to get the dynamic IP(s) allocated at installation time.

## Host IP definition

A few settings are needed to guide the Ansible playbook to compute the settings.

Here is an example you would store in `host_vars/<host>/net.yml` (adding to the list of interfaces):

```yaml
---
net:
  ifaces:
    eth0:
      untagged_vlan: "OSCI-Public"
      tagged_vlans: ["OSCI-Management", "OSCI-Internal"]
    eth1:
      untagged_vlan: "OSCI-Public"
      tagged_vlans: ["OSCI-Management", "OSCI-Internal"]
    eth2:
      untagged_vlan: ["OSCI-example"]
  ips:
    "OSCI-Internal": 172.24.32.6
    "OSCI-Management": 172.24.31.6
    "OSCI-Public": 8.43.85.207
    "OSCI-example": 10.0.1.3
  ips6:
    "OSCI-Public": 2620:52:3:1:d43f:845a:ab32:4821
  bonding:
    0: [eth2, eth3]
```

The `host` declared in the inventory is used to define the hostname.

`ips`/`ips6` defines the host's IP for each VLAN it is part of. There is no need to define a netmask/gateway/… as these parameters are part of the VLAN global definitions in `group_vars/all/network.yml`; these VLANs are no longer limited to the Community Cages.

`bonding` defines the list of bonding interfaces. The key defines the interface name (`bond<key>`) and the value is the interface pair.

Interfaces not part of a bonding will also be configured (default for EL8+ and Debian systems).

## Generating Network Configuration Automatically

When settings are ready, just fire the playbook:

```shell
 ansible-playbook --diff -l <host> playbooks/network_setup.yml

```

If the playbook says:

- `Could not find IP address (not in ARP table)`: most probably the ARP entry expired, so you would need to trigger some network communication with Catton/Spritz (console login + DHCP client restart, reboot, ping dynamic IP from Catton/Spritz…)

## Manual setup

To ensure this playbook will never destroy some specific manual setup (e.g. on machines which could not yet be handled by this method) you can set `manual_setup` to True in `host_vars/<host>/net.yml`.
