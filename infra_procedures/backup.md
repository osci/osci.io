---
title: Backup
---

## Introduction

Services data which cannot be recreated using Ansible deployment need to be saved.

All hosts which are not unmanaged (see the `unmanaged` Ansible group) are monitored by default. But the system needs to be what to backup and if nothing is set then no backup occurs.

The backup is done on BigAldente which holds a SSH key able to connect and copy files. This key does not have shell access and is only able to run rsync and access selected paths to backup. The backup script is run daily by default.

To be able to backup a group of data that needs to be consistent the script can freeze LVM volumes.

## Deployment

Rules with the `backup` tag in `playbooks/common_post.yml` are used to setup an automatic backup.

You don't need any specific Ansible host configuration to enable monitoring but you need to define what data need to be saved.

## Specifying Data to Save

In the inventory, in `host_vars/<host>/backup.yml`, under the `backup` key, backup parameters are defined:

- paths: list or directories or files to save
- frequency: backup frequency (look at `special_time` in `ansible-doc cron` for a list of available values) (defaults to `daily`)
- freeze_lvs: list of `<vg>/<lv>` strings representing LVM volumes to freeze prior to saving files

The `freeze_lvs` parameter can optionally be made a triplet `<vg>/<lv>/<type>` to trigger a special behavior; currently these behaviors (`<type>`) are implemented:

- mysql: initiate a LOCK on all tables server-wide to avoid partial writes during the freeze; once the LVM freeze is in place it is released and the whole process is very short.

For services that are deployed identically (Mail servers, Mailing-lists, hypervisors…) we also often use Ansible groups to define common backup definitions in `group_vars/<group>/backup.yml`; it is of course possible to override these settings in `host_vars/<host>/backup.yml`.
