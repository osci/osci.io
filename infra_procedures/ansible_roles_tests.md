---
title: Ansible Roles Tests
---

## Introduction

To ensure our roles work well and we do not introduce regressions, and also continue to work with new version of Ansible, we test them before accepting changes.

## Framework

To help adding tests to a wider range of difference roles we have established a [testing template](https://gitlab.com/osci/ansible-molecule-tests-template). It is made to prepare the base settings while letting you add your customizations and tests without conflicting.

It is versionned and breaking changes are documented to ease upgrades to newer versions.

It is also unfortunately documenting or adding workarounds for known bugs in the tooling.

## Coverage

Currently only a few roles are fully tested, but this is work in progress. Good examples are the [bind9](https://gitlab.com/osci/ansible-role-bind9) and [httd](https://gitlab.com/osci/ansible-role-ah-httpd) roles.

