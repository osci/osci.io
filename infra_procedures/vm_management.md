---
title: Mailing-Lists
---

- TOC
  {:toc}

## Creating a new VM

VMs are declaredly declaratively in `playbooks/vms.yml` like this:

```
- hosts: jerry.osci.io
  gather_facts: no
  tags: virt
  roles:
    - role: osas_guest_virt_install
      vm_name: example.osci.io
      system_disk_size: 8
      data_disk_size: 30
      mem_size: 2048
      num_cpus: 3
      distribution: Centos
      version: 8
      main_iface_vlan: 'OSCI-Public'
      ifaces_vlans_mapping:
        'OSCI-Public': eth-pub
        'OSCI-Internal': eth-int
      tags: example.osci.io
```

The `osas_guest_virt_install` uses the `guest_virt_install` role to deploy a VM
and tie the network config to our architecture.

The VM base storage (`/dev/vda` of size `system_disk_size` GB) may be
complemented by an extra disk (`/dev/vdb` of size `data_disk_size` GB).
Currently the role is limited to these two drives.

`main_iface_vlan` is the VLAN associated to the default interface. The
list of available VLANs is defined in `group_vars/all/network.yml` in
`cage_vlans`. Each interface is associated to exactly one single VLAN and
`main_iface_vlan` contains the mapping with the interface name; this way the
default interface name may also be specified by giving a name for the VLAN in
used in `main_iface_vlan`.

Resources settings are only defined at creation time. Please update the
playbook to match production in case we need to recreate a VM. Similarly new
network interfaces will be created but removed ones won't be deconfigured.

## Moving a VM on another Hypervisor

Moving a VM is done offline since Hypervisors have no open ports to communicate
with each otherfor security reasons. After several checks the playbook will
recreate the storage, copy all LVM LVs securely, and sync the VM config before
starting the new VM.

The old VM stays around on the source hypervisor and is only stopped with
autostart disabled. After the new VM has been tested and confirmed to work fine
then it can be removed (see `Remove a VM` chapter).

To move a VM between hypervisors use:

```
    ansible-playbook --diff -e hyp_src=<hypervisor1>.osci.io -e hyp_dst=<hypervisor2>.osci.io -e vm_name=examplevm.osci.io playbooks/move_vm.yml
```

## Removing a VM

When a VM is removed all related data are purged:

- libvirt VM config
- provisioning config
- VM storage (all associated LVM LVs)

The VM needs to be manually stopped first as a precaution.

To remove a VM use:

```
    ansible-playbook --diff -e hyp=<hypervisor>.osci.io -e vm_name=examplevm.osci.io playbooks/cleanup_vm.yml
```

## Changing I/O priority of a VM

If a VM is using a lot of I/Os, like a CI node for eg., then it is preferable
to lower the priority for this VM in order to avoid slowing down other
workloads.

The default priority in libvirt is 500 and we use 100 in this situation; we may
define more classes in the future.

This setting is currently set manually but we plan to integrate it in the
`guest_virt_install` role:

```
    virsh blkiotune <vm_name> --weight 100 --config --live
```

To list VMs with a non-default setting:

```
    virsh -q list | awk '{ print $2 }' | while read vm; do weight=$(virsh blkiotune $vm | awk '/weight/ { print $3 }'); if [ $weight -ne 500 ]; then echo $vm: $weight; fi; done
```
