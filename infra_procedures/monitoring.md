---
title: Monitoring
---

## Introduction

All hosts which are not unmanaged (see the `unmanaged` Ansible group) are monitored by default. If is possible to opt in ou out of the default using the `osci_monitoring` parameter.

Hosts, host macros, and host groups are managed by Ansible. Manual changes will be lost.

Templates are linked to the Ansible `zabbix` role and must be kept in sync both with the role and with the repository holding them (see the role documentation).

All configuration fragment and collection scripts are managed by the role too.

Only the following Web UI settings are manually managed:

- global general settings (Administration->General)
- proxy registration (not handled by the role yet) (Administration->Proxies)
- uses and user groups (Administration->Authentication/User groups/Users)
- media types (Administration->Media types)
- server scripts (Administration->Scripts)
- maintenance periods (Configuration->Maintenance)
- actions (Configuration->Actions)

Host discovery and IT services are not used at the moment.

## Deployment

Rules with the `monitoring` tag in `playbooks/common.yml` are used to deploy the Agents and Pollers, and those in `playbooks/common_post.yml` can detect installed services (so you need to run them _after_ all other deployment rules) and act accordingly.

You don't need any specific Ansible host configuration to enable monitoring but you need to add the host to the proper groups (zone groups for example or the poller won't be allowed to poll the agent with the right IP).

## Adding Host Groups

Zabbix host groups are mapped to Ansible host groups. Not all of them are needed though.

Supposing you wish to add a new group for a tenant called `GDB`, you can make it available with the following example configuration in `group_vars/tenant_gdb/monitoring.yml`:

```
---
monitoring_gen:
  host_groups:
    'Tenant GDB':
```

## Limitations

Currently Fedora hosts have trouble running the agent and collection scripts. We are waiting for a fixed package which is currently blocked by a bug, see:

- <https://koji.fedoraproject.org/koji/buildinfo?buildID=1102711>
- <https://bugzilla.redhat.com/show_bug.cgi?id=1568333>
