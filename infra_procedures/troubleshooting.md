---
title: Troubleshooting
---

This is a collection of tips when memory is short :-).

- TOC
{:toc}

## PostgreSQL Corruption "could not read block"

If you get errors like this:

```
could not read block 178 in file "base/119505/119778"
could not read block 21 in file "base/119505/119639"
```

This means the machine was not stopped properly or a filesystem problem. Once
the disks and filesystems have been checked you should be able to locate and
possibly repair the damage.

To find what is broken use `oid2name` with the last number in the error message:

```
# su - postgres
$ oid2name -d mailman -f 119778
From database "mailman":
  Filenode    Table Name
------------------------
    119778  address_pkey
$ oid2name -d mailman -f 119639
From database "mailman":
  Filenode  Table Name
----------------------
    119639   user_pkey
```

In this example these are indexes and they can be fixed easily with:

```
reindex index address_pkey;
reindex index user_pkey;
```

## VM Not Booting

The VM console is accessible using `virsh console <vm_name>` but if the boot
does not go that far then libguestfs comes to the rescue: the `virt-rescue`
tool (`libguestfs-rescue` package on Red Hat systems) allows to access the VM's
OS as if using a rescue media. Common tools to check the filesystem are
available.
